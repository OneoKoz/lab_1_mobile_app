//
//  ViewController.swift
//  Draft_2
//
//  Created by иван on 7.02.22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    var allInfo:[ItemJson]?
    var locGlobItem: ItemJson?
    
    @IBAction func signInGameCenter(_ sender: UIButton) {
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        let uname = usernameTextField.text
        let pass =  (passwordTextField.text!.hashValue as NSNumber).stringValue
        
        for locInfo in allInfo ?? []{
            if uname == locInfo.username,
               pass == locInfo.password{
                locGlobItem = locInfo
                locGlobItem?.isauth = true
                performSegue(withIdentifier: "goVC", sender: nil)
                ResultTableViewController.allData = locInfo.results
                AccViewController.allAchievements = locInfo.achievements
                errorLabel.text = ""
                return
            }
        }
        errorLabel.text = "wrong data"
    }
    
    @IBAction func registration(_ sender: UIButton) {
        let uname:String = usernameTextField.text!
        let pass:String = (passwordTextField.text!.hashValue as NSNumber).stringValue
        
        if uname.isEmpty,
            pass.isEmpty{
            errorLabel.text = "enter all data"
            return
        }
        
        for locInfo in allInfo ?? []{
            if uname == locInfo.username{
                errorLabel.text = "this user is exist"
                return
            }
        }
        
        let newPerson = ItemJson(username: uname, password: pass, isauth: false, results: [], achievements: [])
        self.allInfo?.append(newPerson)
        errorLabel.text = "OK"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.allInfo = uploadSampleData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.writeDataIntoFile(notification:)), name: NSNotification.Name(rawValue: "wrireDataNotificationIntoFile"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.writeDataIntoFile(notification:)), name: NSNotification.Name(rawValue: "wrireDataNotificationIntoFileSave"), object: nil)
        
        for locInfo in allInfo ?? []{
            if locInfo.isauth{
                locGlobItem = locInfo
                performSegue(withIdentifier: "goVC", sender: nil)
                ResultTableViewController.allData = locInfo.results
                AccViewController.allAchievements = locInfo.achievements
                return
            }
        }
    }
    
    func uploadSampleData() -> [ItemJson] {
        var info:[ItemJson] = []
        if let documentDirectory = FileManager.default.urls(for: .documentDirectory,
                                                             in: .userDomainMask).first {
            print(documentDirectory)
            let pathWithFileName = documentDirectory.appendingPathComponent("myJsonData.json")
            if FileManager.default.fileExists(atPath: pathWithFileName.path){
                let data = try! Data(contentsOf: pathWithFileName)
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                do {
                    let itemsJSON = try decoder.decode([ItemJson].self, from: data)
                    info.append(contentsOf: itemsJSON)
                } catch {
                    print(error.localizedDescription)
                }
            }
            
            
            
//            let product: Product = try! JSONDecoder().decode(Product.self, from: data)
            
//            info.append(items)
        }
        
//        if let documentDirectory = FileManager.default.urls(for: .documentDirectory,
//                                                             in: .userDomainMask).first {
//            let pathWithFileName = documentDirectory.appendingPathComponent("myJsonData.json")
//            do {
//                let data = try! Data(contentsOf: pathWithFileName)
//                let product: Product = try! JSONDecoder().decode(Product.self, from: data)
//                let encodedData = try? JSONEncoder().encode(product)
//                print(pathWithFileName)
//                print(type(of: pathWithFileName))
//                print("hi")
//                try encodedData!.write(to: pathWithFileName)
//            } catch {
//                print("error")
//            }
//        }
        return info
        
    }
    
    @objc func writeDataIntoFile(notification: Notification){
        if notification.name.rawValue == "wrireDataNotificationIntoFile" {
            locGlobItem?.isauth = false
        }
        locGlobItem?.results = ResultTableViewController.allData
        locGlobItem?.achievements = AccViewController.allAchievements
        allInfo?.removeAll(where: { curItem in
            curItem.username == locGlobItem?.username
        })
        allInfo?.append(locGlobItem!)
        usernameTextField.text=""
        passwordTextField.text=""
        if let documentDirectory = FileManager.default.urls(for: .documentDirectory,
                                                            in: .userDomainMask).first {
            let pathWithFileName = documentDirectory.appendingPathComponent("myJsonData.json")
            do {
                let encoder = JSONEncoder()
                encoder.keyEncodingStrategy = .convertToSnakeCase
                let encodedData = try? JSONEncoder().encode(allInfo)
                try encodedData!.write(to: pathWithFileName)
                
            } catch {
                        print("error")
                    }
        }
    }

}

