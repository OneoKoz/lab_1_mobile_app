//
//  ResultTableViewController.swift
//  Draft_2
//
//  Created by иван on 8.02.22.
//

import UIKit

class ResultTableViewController: UITableViewController {
    
    static var allData:[ItemJson.ResultOne] = []
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print(ResultTableViewController.allData.count)
        return ResultTableViewController.allData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SimpleTableViewCell
        theCell.dateLabel.text = ResultTableViewController.allData[indexPath.row].date
        theCell.scoreLabel.text = String(ResultTableViewController.allData[indexPath.row].score)
        return theCell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.shouldReload), name: NSNotification.Name(rawValue: "newDataNotificationForItemEdit"), object: nil)
    }
    
    @objc func shouldReload() {
        self.tableView.reloadData()
    }

}
