//
//  ResultsViewController.swift
//  Draft_2
//
//  Created by иван on 7.02.22.
//

import UIKit

class ResultsViewController: UIViewController, UITableViewDataSource{
    
    static var allData:[ItemJson.ResultOne] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(ResultsViewController.allData.count)
        return ResultsViewController.allData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SimpleTableViewCell
        theCell.dateLabel.text = ResultsViewController.allData[indexPath.row].date
        theCell.scoreLabel.text = ResultsViewController.allData[indexPath.row].score
        return theCell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

extension ResultsViewController: UpdatesDelegate, UITableViewDelegate {
    func didFinishUpdates(finished: Bool) {
        guard finished else {
            // Handle the unfinished state
            return
        }
        
    }
}
