//
//  File.swift
//  Draft_2
//
//  Created by иван on 7.02.22.
//

import Foundation

struct Menu{
    var header: String!
    var row: [String]!
    
    init(header: String, row:[String]!) {
        self.header = header
        self.row = row
    }
}
