//
//  AccViewController.swift
//  Draft_2
//
//  Created by иван on 7.02.22.
//

import UIKit

class AccViewController: UIViewController, UITableViewDataSource{
    
    static var allAchievements:[ItemJson.AchievementOne] = []
    
    @IBOutlet weak var tableAchievements: UITableView!
    
    @IBAction func exit(_ sender: UIButton!) {
        _ = navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name("wrireDataNotificationIntoFile"), object: nil)
    }
    
    
    @IBAction func addNewAchievement(_ sender: UIButton) {
        let alertController = UIAlertController(title: "NEW ACHIEVEMENT", message: "enter data", preferredStyle: .alert)
        
        alertController.addTextField { descrTF in
            descrTF.placeholder = "description"
        }
        alertController.addTextField { scoreTF in
            scoreTF.placeholder = "score"
        }
        
        let alertOkBtn = UIAlertAction(title: "ADD", style: .default) { [weak alertController](_) in
            if alertController?.textFields![0].text?.isEmpty != true,
               alertController?.textFields![1].text?.isEmpty != true {
                
                let newDescr = alertController?.textFields![0].text
                let newScore = Int((alertController?.textFields![1].text!)!)
                
                print(AccViewController.allAchievements)
                AccViewController.allAchievements.append(ItemJson.AchievementOne(descripion: newDescr!, score: newScore!, isachieved: false))
                print(AccViewController.allAchievements)
                self.tableAchievements.reloadData()
            }
        }
        alertController.addAction(alertOkBtn)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @objc func addNewItem() {
        tableAchievements.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.addNewItem), name: NSNotification.Name(rawValue: "newItemAchieevementNotification"), object: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theCell = tableView.dequeueReusableCell(withIdentifier: "cellAcgievement") as! AchievementTableViewCell
        theCell.descriptionTextView.text = AccViewController.allAchievements[indexPath.row].descripion
        theCell.scoreDescrLabel.text = String(AccViewController.allAchievements[indexPath.row].score)
        theCell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        if AccViewController.allAchievements[indexPath.row].isachieved{
            theCell.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        }
        return theCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AccViewController.allAchievements.count
    }

}
