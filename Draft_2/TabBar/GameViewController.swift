//
//  GameViewController.swift
//  Draft_2
//
//  Created by иван on 7.02.22.
//

import UIKit

class GameViewController: UIViewController {
    
    
    @IBOutlet weak var catImage: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var startbnt: UIButton!
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBAction func startGame(_ sender: UIButton) {
        score = 0
        scoreLabel.text = String(score)
        hideBtn(isHide: true)
    }
    
    func hideBtn (isHide:Bool){
        startbnt.isHidden = isHide
        shareBtn.isHidden = isHide
        stopBtn.isHidden = !isHide
        btn1.isHidden = !isHide
        btn2.isHidden = !isHide
        btn3.isHidden = !isHide
        displayLink.isPaused = !isHide
    }
    
    @IBAction func feed(_ sender: UIButton) {
        
        if (sender.tintColor != #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) ){
            return
        }
        score+=1
        scoreLabel.text = String(score)
        if (score % 15==0){
            self.catImage.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.catImage.stopAnimating()
                }
        }
        var k = 0
        for locAchievment in AccViewController.allAchievements{
            if !locAchievment.isachieved && locAchievment.score == score{
                AccViewController.allAchievements[k].isachieved = true
            }
            k+=1
        }
        NotificationCenter.default.post(name: NSNotification.Name("newItemAchieevementNotification"), object: nil)
    }
    
    @IBAction func stopGame(_ sender: Any) {
        hideBtn(isHide: false)
        let newItem = ItemJson.ResultOne(date: dateFormatter.string(from: Date.now), score: Int(scoreLabel.text!)!)
        ResultTableViewController.allData.append(newItem)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newDataNotificationForItemEdit"), object: nil)
    }
    
    @IBAction func share(_ sender: UIButton) {
        //share button
        let activityController = UIActivityViewController(activityItems: [scoreLabel.text!], applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func showResult(_ sender: UIButton) {
    }
    
    var score: Int = 0
    var displayLink: CADisplayLink!
    
    let dateFormatter = DateFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoreLabel.text = String(score)
        catImage.animationImages = [#imageLiteral(resourceName: "1page"), #imageLiteral(resourceName: "2page"), #imageLiteral(resourceName: "3page"), #imageLiteral(resourceName: "4page"), #imageLiteral(resourceName: "5page")]
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        dateFormatter.locale = Locale.current
        
        displayLink = CADisplayLink(target: self, selector: #selector(self.setRandomBackgroundColor))
        displayLink.preferredFrameRateRange = CAFrameRateRange(minimum: 2, maximum: 4, __preferred: 3)
        displayLink?.add(to: .main, forMode: .common)
        displayLink.isPaused = true
    }
    
    @objc func setRandomBackgroundColor() {
        let allGameBtn = [btn1, btn2, btn3]
        let index = Int(arc4random_uniform(UInt32(3)))
        for k in 0...2 {
                allGameBtn[k]?.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        allGameBtn[index]!.tintColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
    }
                                                         

}
