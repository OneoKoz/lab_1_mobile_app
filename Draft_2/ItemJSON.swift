//
//  ItemJSON.swift
//  Draft_2
//
//  Created by иван on 7.02.22.
//

import Foundation

struct ItemJson: Codable {
    struct ResultOne: Codable{
        var date: String
        var score: Int
    }
    
    struct AchievementOne: Codable{
        var descripion: String
        var score: Int
        var isachieved: Bool
    }
    
    var username: String
    var password: String
    var isauth: Bool
    var results: [ResultOne]
    var achievements:[AchievementOne]
}
